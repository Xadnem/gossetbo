El propòsit d’aquesta aplicació es controlar des del telefon mòbil, l’estat del pany d'una porta i visualitzar les imatges proporcionades per una càmera de vídeo, en aquest cas una Raspberry camera.

Quan es tanca el pany de la porta, el sistema queda armat i si el pany es torna a obrir ( o es forçat ) sense introduir prèviament un codi a la aplicació del mòbil, el sistema envia un missatge de Telegram al mòbil del usuari, i activa la càmera a dins del recinte darrera el pany.

L'aplicació consta de tres programaris diferents:

- Programari Python del sistema de control central carregat a una Raspberry
- Programari Android per a l’aplicació al telefon.
- Programari C++ que es carrega a una placa ESP32

El sistema utilitza el protocol Mqtt per a la comunicació entre dispositius.

Podeu trobar informació detallada, al fitxer Manual_GossetBo.pdf

