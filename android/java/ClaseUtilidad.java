package GbControlA.GbControlA;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public class ClaseUtilidad
{
    static AlertDialog dialog;


    public static void mostrarMessageBox(Context contexto, String titulo, String mensaje)
    {
        dialog = new AlertDialog.Builder(contexto) // Pass a reference to your main activity here
                .setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.cancel();
                    }
                })
                .show();
    }



}
