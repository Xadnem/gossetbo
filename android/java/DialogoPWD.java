package GbControlA.GbControlA;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import androidx.appcompat.app.AppCompatActivity;

public class DialogoPWD extends AppCompatActivity
{
    private EditText etPwd;
    private Intent pantallaconfiguracion,inicio;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialogopassword);
        context = this;
        pantallaconfiguracion = new Intent(getApplicationContext(),PantallaConfiguracion.class);
        inicio = new Intent(getApplicationContext(),MainActivity.class);
        etPwd = findViewById(R.id.etPwd);

        Button btAceptar = findViewById(R.id.btAceptar);
        btAceptar.setBackgroundColor(Color.rgb(102,255,102));
        btAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean ok = comprobarPWD();
                if(ok)
                {
                    startActivity(pantallaconfiguracion);
                }
                else
                    ClaseUtilidad.mostrarMessageBox(context,"Error",getResources().getString(R.string.errorPwd));
            }
        });
        Button btCancelar = findViewById(R.id.btCancelar);
        btCancelar.setBackgroundColor(Color.rgb(255,102,102));
        btCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(inicio);
            }
        });
    }

    ///////////////////////////////
    //
    //  RETORNA TRUE SI EL PASSWORD DEL BROKER INTRODUIT AL EDITTEXT ES CORRESPON AMB EL QUE HI HA AL
    //  FITXER DE CONFIGURACIO I FALSE EN CAS CONTRARI
    //
    ////////////////////////////////////////////
    //
    private boolean comprobarPWD()
    {
        String pwd = etPwd.getText().toString();
        if(pwd.equals(GestorArchivos.getPWD()))
            return true;
        return false;
    }

}
