package GbControlA.GbControlA;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;


public class PantallaConfiguracion extends AppCompatActivity
{
    EditText tbIpBroker,tbPort,tbTopicCodigo,tbCodigo,tbUser,tbTopicRecepcion,tbCodigoDesactivacion;
    Button btAceptar;
    Context context;
    Intent anterior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutconfiguracion);
        context = this;
        anterior = new Intent(getApplicationContext(),MainActivity.class);

        tbIpBroker = findViewById(R.id.tbIpBroker);
        tbPort = findViewById(R.id.tbPort);
        tbTopicCodigo = findViewById(R.id.tbTopicCodigo);
        tbCodigo = findViewById(R.id.tbCodigo);
        tbUser = findViewById(R.id.tbUser);
        tbTopicRecepcion = findViewById(R.id.tbTopicRecepcion);
        tbCodigoDesactivacion = findViewById(R.id.tbCodigoDesactivacion);
        btAceptar = findViewById(R.id.btAceptar);
        // Omplir les capses de text amb el contingut actual del fitxer de configuració.
        String ip = GestorArchivos.getIP();
        tbIpBroker.setText(ip);
        String port = GestorArchivos.getPORT();
        tbPort.setText(port);
        String topic = GestorArchivos.getTOPIC();
        tbTopicCodigo.setText(topic);
        String password = GestorArchivos.getPWD();
        tbCodigo.setText(password);
        tbUser.setText(GestorArchivos.getUSER());
        tbCodigoDesactivacion.setText(GestorArchivos.getCODIGODESACTIVACION());
        tbTopicRecepcion.setText(GestorArchivos.getTOPICRECEPCION());
    }


    //////////////////////////////////
    //
    // OBTÉ EL CONTINGUT DE LES CAIXES DE TEXT. EN CAS DE HAVER-HI ALGUNA BUIDA, MOSTRA UN MISSATGE
    // D'ERROR I RETORNA.
    // SI TOTES LES CAPSES DE TEXT ESTAN PLENAS, CRIDA EL MÈTODE comprobarDatos DE GestorDeArchivos
    // I LI PASSA UN HASHMAP AMB LES DADES COM ARGUMENT, SI GestorDeArchivos RETORNA true CRIDA A
    // escribirConfiguracion DE gestorDeArchivos
    //
    /////////////////////////////////////////////////
    //
    public void btAceptar_onClick(View view) throws IOException {
        // Comprovar què no hi ha capses de text buides i les dades de ip i topic són vàlids.
        String ipkey = getResources().getString(R.string.datosIp);
        String ip = tbIpBroker.getText().toString();
        String portkey = getResources().getString(R.string.datosPort);
        String port = tbPort.getText().toString();
        String topickey = getResources().getString(R.string.datosTopicCodigo);
        String topic = tbTopicCodigo.getText().toString();
        String codigokey = getResources().getString(R.string.datosCodigo);
        String codigo = tbCodigo.getText().toString();
        String topicrecepcionkey = getResources().getString(R.string.datosTopicRecepcion);
        String topicrecepcion = tbTopicRecepcion.getText().toString();
        String userkey = getResources().getString(R.string.datosUser);
        String user = tbUser.getText().toString();
        String codigodesactivacionkey = getResources().getString(R.string.datosCodigoDesactivacion);
        String codigodesactivacion = tbCodigoDesactivacion.getText().toString();

        boolean vacia = (ip.length() == 0 ||port.length() == 0 || topic.length() == 0 || codigo.length() == 0||user.length() == 0);
        if(vacia)
        {
            ClaseUtilidad.mostrarMessageBox(this,getResources().getString(R.string.tituloError),
                    getResources().getString(R.string.errorCajaVacia));
            return;
        }
        else if(!comprobarDatos(ipkey,ip))
        {
            ClaseUtilidad.mostrarMessageBox(this,getResources().getString(R.string.tituloError),getResources().getString(R.string.errorIp));
            return;
        }
        else if(!comprobarDatos(portkey,port))
        {
            ClaseUtilidad.mostrarMessageBox(this,getResources().getString(R.string.tituloError),getResources().getString(R.string.errorPort));
            return;
        }
        else if(!comprobarDatos(topickey,topic))
        {
            ClaseUtilidad.mostrarMessageBox(this,getResources().getString(R.string.tituloError),getResources().getString(R.string.errorTopic));
            return;
        }
        // Posar les dades en un Hashmap i cridar a escribirConfituracion
        HashMap <String,String> datos = new HashMap <>();
        datos.put(ipkey,ip);
        datos.put(portkey,port);
        datos.put(topickey,topic);
        datos.put(topicrecepcionkey,topicrecepcion);
        datos.put(codigokey,codigo);
        datos.put(userkey,user);
        datos.put(codigodesactivacionkey,codigodesactivacion);
         try {
             GestorArchivos.escribirConfiguracion(datos,getResources().getString(R.string.datosRuta),context);
         }
         catch(IOException e)
         {
             ClaseUtilidad.mostrarMessageBox(this,getResources().getString(R.string.tituloError),
                     getResources().getString(R.string.errorEscritura));
         }
        // Reasignar el valor dels atributs de la clase GestorArchivos
        File ficheroconfiguracion = new File(this.getFilesDir(),getResources().getString(R.string.datosRuta));
        GestorArchivos.leerConfiguracion(ficheroconfiguracion,this);

        // Tornar a la pantalla principal posant true en el boolea del bundle
        ClaseUtilidad.mostrarMessageBox(this,"OK",getResources().getString(R.string.ficheroescrito));
        anterior.putExtra("DatosOK",true);
        startActivity(anterior);
    }

    ////////////////////////////////////////
    //
    // TORNA TRUE SI ELS VALORS A LES CAPSES DE TEXT SON VALIDS I FALSE EN CAS CONTRARI
    //
    //////////////////////////////////
    //
    private boolean comprobarDatos(String key,String valor)
    {
        if(key.equals(getResources().getString(R.string.datosIp)))
        {
            if(!Character.isDigit(valor.charAt((0))) || !Character.isDigit(valor.charAt(valor.length()-1))
               || cuentaPuntos(valor) != 3 || !numerosYPuntos(valor) || valor.length() < 7 || valor.length() > 15)
                return false;
        }
        else if(key.equals(getResources().getString(R.string.datosPort)))
        {
            try {
                int nport = Integer.parseInt(valor);
            }
            catch(NumberFormatException e)
            {
                return false;
            }
        }
        else if(key.equals(getResources().getString(R.string.datosTopicCodigo)))
            if(valor.charAt(0) == '$')
                return false;
        return true;
    }

    ///////////////////////////////////////
    //
    // TORNA LA QUANTITAT DE CARACTERS QUE SON UN PUNT A LA IP PASSADA COM A ARGUMENT
    //
    /////////////////////////////////
    //
    private int cuentaPuntos(String ip)
    {
        int cont = 0;
        for(int i = 0; i < ip.length();i++)
            if(ip.charAt(i) == '.')
                cont++;
            return cont;
    }
    ////////////////////////////////////
    //
    // TORNA TRUE SI TOTS ELS CARACTERS A LA IP PASSADA COM ARGUMENT SON UN PUNT O UN NOMBRE I FALSE
    // EN CAS CONTRARI.
    //
    /////////////////////////////////////////
    //
    private boolean numerosYPuntos(String ip)
    {
        for(int i = 0; i < ip.length();i++)
            if(!Character.isDigit(ip.charAt(i)) && ip.charAt(i) != '.' )
                return false;
        return true;
    }


}


