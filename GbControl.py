#!/usr/bin/python3.7

from GbSecretTopic import *
from GbGUI import GbGUI
from GbMqtt import GbMqtt
from os import system,path
import subprocess
import time


class GbControl:
    def __ini__(self):
        # Instanciació d'atributs de la classe
        self.armat = False
        self.warning = False
        self.estatboto = 0
        self.p = None
        self.temps = 300  # Temps per a obrir la porta quan s'introdueix el codi en segons.
        self.tempsestablert = 0  # Temps en segons en el que es desactivarà el sistema, a partir d'aquest moment hi ha
                                 #  "temps" segons fins que el sistema es torna a armar

        # Asignar la ruta del fitxer de configuració a la variable ruta de GbSecretTopic
        GbSecretTopic.ruta =   path.dirname(path.realpath(__file__)) + path.sep + 'config.ini'
        # Inicialització de la clase GbSecretTopic amb les dades dels topics , codi de desactivacio i ip's
        GbSecretTopic.establirSecrets()

        # Creació de objecte GbGUI
        self.gui = GbGUI()
        # Creacio d'un objecte GbMqtt subscrit als topic de control
        llistaTopics = [GbSecretTopic.subsboto, GbSecretTopic.subenviamentcodi, GbSecretTopic.subsonoff,
                        GbSecretTopic.codienviat, GbSecretTopic.activacamara,
                        GbSecretTopic.comunicaestat,GbSecretTopic.consultacodi,GbSecretTopic.codicanviat]
        GbMqtt.broker = GbMqtt(llistaTopics)
        GbMqtt.broker.establirUserPassword(GbSecretTopic.username, GbSecretTopic.pwd)
        GbMqtt.broker.connectarABroker(GbSecretTopic.broker, GbSecretTopic.port, GbSecretTopic.ta)
        GbMqtt.broker.on_message = self.on_message_rebut
        GbMqtt.broker.on_connect = self.quan_connectat
        GbMqtt.broker.on_disconnect = self.quan_desconnectat

        # Reiniciar la placa ESP32
        GbMqtt.publicaEnBroker("RestartESP","reinici")

        GbMqtt.broker.loop_start()
        self.gui.root.mainloop()

    def on_message_rebut(self, client, userdata, msg):
        """ Métode de gestió del event de rebuda de missatges pel objete mqtt"""
        print(f"{msg.topic}{msg.payload}")
        if msg.topic == GbSecretTopic.estatboto:  # Quan es rebi l'estat del pany des de la ESP32
            self.gestionaEstatBoto(msg)
        elif msg.topic == GbSecretTopic.codienviat:  # Enviament del codi de desactivació des de un mobil
            self.gestionaCodi(msg)
        elif msg.topic == GbSecretTopic.activacamara: # Missatges d'activació i desactivació de la càmera
            if self.p is None and msg.payload.decode("utf-8") == "CamaraOn":
              self.activaCamara()
            elif msg.payload.decode("utf-8") == "CamaraOff":
               self.desactivaCamara()
        elif msg.topic == GbSecretTopic.comunicaestat and msg.payload.decode("utf-8") == "diguesComEstas": # Petició de estat actual per part d'un mobil
            if self.warning:
                GbMqtt.publicaEnBroker(GbSecretTopic.comunicaestat, "Warning")
            elif self.armat:
                GbMqtt.publicaEnBroker(GbSecretTopic.comunicaestat, "Armat")
            elif not self.armat:
                GbMqtt.publicaEnBroker(GbSecretTopic.comunicaestat, "Desarmat")
        elif msg.topic == GbSecretTopic.consultacodi and msg.payload.decode("utf-8") == "ConsultaCodi": # Petició del codi actual per un mobil
            GbMqtt.publicaEnBroker(GbSecretTopic.consultacodi,GbSecretTopic.codi)
        elif msg.topic == GbSecretTopic.codicanviat: # Quan es canvia el codi de desactivació a un mobil
            GbSecretTopic.canviarCodi(msg.payload.decode("utf-8"))
            print("Codi de desactivació canviat")

    def quan_connectat(self, client, userdata, flags, rc):

        """ Mostra l'estat de conexió amb el broker a l'etiqueta de la GUI quan es connecta amb el broker """
        if rc == 0:
            GbMqtt.broker.subscriureTopics()
            GbMqtt.broker.connectat = True
            self.gui.lbConexio.configure(bg="Chartreuse", text="Connectat")
            print("Connectat")
        else:
            print(f"(Conexió fallida amb codi: {rc}")
            GbMqtt.broker.connectat = False
            self.gui.lbConexio.configure(bg="Red", text="No connectat")

    def quan_desconnectat(self,client,userdata,rc):
        if rc != 0:
            GbMqtt.broker.connectat = False
            self.gui.lbConexio.configure(bg="Red", text="No connectat")

    def gestionaEstatBoto(self,msg):

        """ Crida al métode adient en funció del estat del sistema i del pany de la porta """
        self.estatboto = int(msg.payload)
        # Si el sistema esta desactivat i el pany tancat, tornar a activar després de "temps"
        if not self.armat and self.estatboto == 1:
            self.armaDespresDeTemps()
        # Si el sistema està activat i s'obre el pany de la porta, posar el sistema en estat de Warning
        elif self.armat and self.estatboto == 0:
            self.estableixWarning()
        elif not self.armat and self.estatboto == 0:
            self.tempsestablert = 0

    def armaDespresDeTemps(self):

        """ Torna a armar el sistema quan pasa el temps establert a la variable tempsestablert de la clase"""
        ara = time.time()
        print("Ara" + str(ara) + " Establert: " + str(self.tempsestablert) + "  Compte: " + str(
            self.tempsestablert + self.temps - ara))
        # Si ha passat el temps establert, posar la variable a zero
        if (self.tempsestablert > 0) and ((self.tempsestablert + self.temps - ara) < 0):
            self.tempsestablert = 0
        # Si ha pasat el temps establert, tornar a armar el sistema
        if self.tempsestablert == 0:
            self.armat = True
            self.gui.lbEstat.config(text="Sistema activat")
            GbMqtt.publicaEnBroker(GbSecretTopic.comunicaestat, "Armat")
            print("Enviado Topic: " + GbSecretTopic.comunicaestat + " Mensaje  Armat")

    def estableixWarning(self):

        """ Mostra el missatge de Warning a la GUI, crida al bot de Telegram, posa el Sonoff a ON, activa
        la càmera i envia el estat de Warning als dispositius mobils. """
        self.gui.lbEstat.config(text="¡¡ WARNING !! ")
        self.warning = True
        system("sh probaBot/bot.sh")
        GbMqtt.publicaEnBroker(GbSecretTopic.comunicaestat, "Warning")
        # Activar la camera i el servidor http en un altre fil
        self.activaCamara();
        self.armat = False

    def gestionaCodi(self,msg):

        """ Comproba que el codi de desactivació sigui correcte i si ho es, desactiva el sistema,
        posa el Sonoff a OFF, desactiva la càmera, mostra el nou estat a la etiqueta de la GUI i
        envia la confirmació de codi correcte."""
        if msg.payload.decode("utf-8") == GbSecretTopic.codi:
            self.gui.lbEstat.config(text="Sistema desactivat ")
            self.warning = False
            self.tempsestablert = time.time()
            GbMqtt.publicaEnBroker(GbSecretTopic.comunicaestat, "Desarmat")
            GbMqtt.publicaEnBroker(GbSecretTopic.codiok, "Codi correcte")
            self.desactivaCamara()
            self.armat = False
        else:
            GbMqtt.publicaEnBroker(GbSecretTopic.codiok, "Codi incorrecte")

    def activaCamara(self):

        """ Activa la camera i el servidor HTTP pel enviament d'imatges en un altre process """
        if self.p is not None:
            self.p.kill()
            self.p = None
        # self.p = subprocess.Popen(["sh","motion.sh"])
        self.p = subprocess.Popen(['python3', 'GbCamera.py'])
        GbMqtt.publicaEnBroker(GbSecretTopic.subsonoff, "ON")
        # Obrir el navegador per veure el contingut de la càmera
        # self.p2 = subprocess.Popen(['chromium-browser', 'http://127.0.0.1:8000'])
        # self.p2 = subprocess.Popen(['firefox','http://127.0.0.1:8000'])

    def desactivaCamara(self):
        GbMqtt.publicaEnBroker(GbSecretTopic.sonoff, "OFF")
        if self.p is not None:
            self.p.kill()
            self.p = None


""" Main """
gbcontrol = GbControl()
gbcontrol.__ini__()
