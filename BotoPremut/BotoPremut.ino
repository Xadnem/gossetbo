#include "GestorFicheros.h"
#include "WifiConnect.h"
#include "MqttConnect.h"

#define pin 5
#define largofichero 1000
#define pinboto 2

char * configuracion;
int estatboto;

void setup() {

   pinMode(pinboto,INPUT_PULLUP); 
   Serial.begin(115200);
   
   /* Comprobracion y apertura del stream a la tarjeta SD */
   if(compruebaTarjeta(pin))
    Serial.println("Tarjeta OK");
   else
    Serial.println("Error al abrir la tarjeta.");
    
    /* Obtener el String correspondiente al Json con los datos de configuracion
     *  en la tarjeta */
    configuracion = (char*)malloc(largofichero);
    leerFichero("/Datos.txt",configuracion,largofichero);
    String configuracions =(String)configuracion;
    //Serial.println("Datos.txt");
    //Serial.println(configuracion);    
    //Serial.println("Config " + configuracions);
    
    /* Deserializar el json y meter los datos en una array de cadenas */
    const char ** lista = (const char**)malloc(sizeof(char*)*10);
    deserializaJson(configuracions,lista); 
    //Serial.println("DATOS:");
    //for(int i = 0; i < 6;i++)
      //Serial.println(lista[i]);  
      
    /* Establecer el valor de los datos de conexion a la red wifi */
    setSsid(lista[0]);
    setPwd(lista[1]);
    
    /* Establecer el valor de los datos de conexion al broker */
    setMqtt_server(lista[2]);
    setMqtt_port(lista[5]);
    setMqtt_user(lista[4]);
    setMqtt_pwd(lista[3]);  
    setMqtt_topic(lista[6]);
    
    /* Declaracion de tareas de FreeRTOS */   
    xTaskCreate(
                    conectaWifi,      // Task function. 
                    "conectaWifi",    // String with name of task. 
                    10000,            // Stack size in bytes. 
                    NULL,             // Parameter passed as input of the task 
                    1,                // Priority of the task. 
                    NULL);            // Task handle. 
                   

  /* Establecer los valores iniciales del broker */
    setupMqtt();
    

    xTaskCreate(
          mqttConnect,      // Task function. 
          "mqttConnect",    // String with name of task. 
          10000,            // Stack size in bytes. 
          NULL,             // Parameter passed as input of the task 
          1,                // Priority of the task. 
          NULL);            // Task handle. 

  /* Comprobar el estado del boton */
     xTaskCreate(
          publicaEstadoBoton,      // Task function. 
          "publicaEstadoBoton",    // String with name of task. 
          10000,            // Stack size in bytes. 
          NULL,             // Parameter passed as input of the task 
          1,                // Priority of the task. 
          NULL);            // Task handle. 
 
   
   /*
    // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt...");
    myFile.println("testing 1, 2, 3.");
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
*/
  
}


/////////////////////////////////
//
//  COMPRUEBA EL ESTADO DEL BOTON Y CAMBIA EL VALOR
//  DE LA VARIABLE EN CONSECUENCIA
//
//////////////////////////////////////
//
void comprobaBoto()
{
  if(digitalRead(pinboto) == 1)
  {
    estatboto = 0;
    //Serial.println("Botó no premut");    
  }
  else
  {
    Serial.println("Botó premut");
    estatboto = 1;
  }  
}

///////////////////////////////
// 
// PUBLICA EL ESTADO DEL BOTON EN EL BROKER
//
/////////////////////////////////////////
//
void publicaEstadoBoton(void * parameter)
{ 
  while(true)
  { 
    if(estatConexio())//Si el cliente Mqtt esta conectado
    {
      comprobaBoto();
      char estat[2];
      sprintf(estat, "%d", estatboto);
      // Enviament del missatge amb l'estat del botó al topic d'estat
      publicaMensaje(getTopic(), estat);
      vTaskDelay(1000); 
    }
  }  
  vTaskDelete( NULL );
}
// Fi gestionaPubSub()

void loop()
{
  
}
