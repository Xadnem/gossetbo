#!/usr/bin/python3.8

import tkinter
from tkinter import *
from tkinter import messagebox
import subprocess
from GbSecretTopic import *
from tkcalendar import *
import datetime
from GbMqtt import GbMqtt


class GbGUI:
    def __init__(self):

        # Ventana gràfica per encabir la resta de ginys
        self.root = tkinter.Tk()
        alto, ancho = self.root.winfo_screenheight(), self.root.winfo_screenwidth()
        self.root.title("Control de GossetBo")
        self.root.geometry("%dx%d+0+0" % (ancho, alto))

        # Definicions de tipus de lletra i colors
        altofontRetols = int(alto * 0.015)
        altofontAnotacions = int(alto * 0.01)
        fons = "SeaGreen"
        fonscapses = "LightGreen"
        fontRetols = ("Arial", altofontRetols, "underline")
        fontText = ("Arial", altofontRetols)
        fontAnotacions = ("Arial", altofontAnotacions)
        self.root.configure(bg=fons)

        # Unitats en funció del alt i ample de la pantall per a situar els widgets
        unialto = int(alto * 0.01)
        # Etiqueta retol de l'estat del sistema de control
        retolControl = Label(self.root, text='Estat del sistema')
        retolControl.configure(font=fontRetols, bg=fons)
        retolControl.place(x=90, y=unialto * 2)

        # Etiqueta per a mostrar l'estat del sistema de control
        self.lbEstat = Label(self.root, text='Sistema desactivat')
        self.lbEstat.pack()
        self.lbEstat.place(x=90, y=unialto * 6, height=altofontRetols * 2, width=ancho * 0.15)
        self.lbEstat.configure(font=fontText)

        # Etiqueta per a mostrar l'estat de conexió
        self.lbConexio = Label(self.root, text='')
        self.lbConexio.place(x=ancho - ancho * 0.1 - 20, y=unialto * 2, height=altofontRetols * 2, width=ancho * 0.1)

        # Etiqueta per al retol de canvi de codi de desactivació
        retolCanviCodi = Label(self.root, text='Canvi del codi de desactivació')
        retolCanviCodi.configure(font=fontRetols, bg=fons)
        retolCanviCodi.place(x=75, y=unialto * 25)

        # Capsa de text per a introduir el codi actual
        self.tbCodiActual = Entry(self.root);
        self.tbCodiActual.configure(font=fontText, bg=fonscapses)
        anchocapsa = ancho * 0.15
        self.tbCodiActual.place(x=75, y=unialto * 29, width=anchocapsa)

        # Etiqueta per a l'anotació del codi actual
        anotacioCodiActual = Label(self.root, text='Codi actual')
        anotacioCodiActual.configure(font=fontAnotacions, bg=fons)
        anotacioCodiActual.place(x=75 + anchocapsa + 10, y=unialto * 29)

        # Capsa de text per a introduir el nou codi
        self.tbNouCodi = Entry(self.root)
        self.tbNouCodi.configure(font=fontText, bg=fonscapses)
        self.tbNouCodi.place(x=75, y=unialto * 35, width=anchocapsa)

        # Etiqueta per a l'anotació del nou codi
        anotacioNouCodi = Label(self.root, text='Nou codi')
        anotacioNouCodi.configure(font=fontAnotacions, bg=fons)
        anotacioNouCodi.place(x=75 + anchocapsa + 10, y=unialto * 35)

        # Botó per a aceptar el nou codi
        btCanviCodi = Button(self.root, text="Acceptar", bg="GreenYellow", command=self.onBtCanviCodi_Clicked)
        btCanviCodi.place(x=165, y=unialto * 40)

        lbRetolGrafana = Label(self.root, text="Gràfics", bg="seagreen", fg="Black",
                               justify="left", anchor=W, font=fontRetols)
        lbRetolGrafana.place(x=90, y=unialto * 54)

        # Etiqueta pel retol del calendari de la data d'inici del gràfic
        lbDataInici = Label(self.root, text="Inici de periode", bg="seagreen", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        anchocal = int(ancho * 0.06)
        lbDataInici.place(x=90 + anchocal + 5, y=unialto * 58)

        # EntryCalendar per a la data d'inici
        self.calendariinici = DateEntry(self.root, font=fontText)
        self.calendariinici.place(x=90, y=unialto * 58, width=anchocal)

        # # Etiqueta pel retol del calendari de la data de fi del gràfic
        lbDataFi = Label(self.root, text="Fi de periode", bg="seagreen", fg="Black",
                         justify="left", anchor=W, font=fontAnotacions)
        lbDataFi.place(x=90 + anchocal + 5, y=unialto * 63)

        # Mostrar el EntryCalendar per a la data de fi del periode
        self.calendarifi = DateEntry(self.root, font=fontText)
        self.calendarifi.place(x=90, y=unialto * 63, width=anchocal)

        # Botó per a obrir el gràfic de consum d'aigua a Grafana
        anchoboton = int(ancho * 0.05)
        btAigua = Button(self.root, text="Aigua", bg="LightBlue", command=self.onBtGrafanaA_Clicked)
        btAigua.place(x=90, y=unialto * 68, width=anchoboton)

        # Botó per obrir el gràfic de consum de llum a Grafana
        btLlum = Button(self.root, text="Llum", bg="RoyalBlue", command=self.onBtGrafanaL_Clicked)
        btLlum.place(x=90 + anchoboton + 10, y=unialto * 68, width=anchoboton)

    def onBtCanviCodi_Clicked(self):

        """ Comprova que s'han omplert les dues capses de text del codi actual i el nou. Si el canvi es porta a terme
         es mostra un missatge i es publica en el broker per a ser canviat a tots els dispositius mobils associats."""
        if self.tbCodiActual.get() != GbSecretTopic.codi:
            messagebox.showerror("Error", "Codi actual erroni")
        elif len(self.tbNouCodi.get()) == 0:
            messagebox.showerror("Error", "El nou codi no pot estar buit")
        else:
            codi = self.tbNouCodi.get()
            canviat = GbSecretTopic.canviarCodi(codi)
            if canviat:
                messagebox.showinfo("OK", "Codi de desactivació canviat")
                self.tbCodiActual.delete(0, len(self.tbCodiActual.get()))
                self.tbNouCodi.delete(0, len(self.tbNouCodi.get()))
                GbMqtt.publicaEnBroker(GbSecretTopic.codicanviat,codi)
            else:
                messagebox.showerror("Error", "No s'ha canviat el codi de desactivació." + GbSecretTopic.codi)

    """ Per a futures versions """
    def onBtGrafanaA_Clicked(self):
        # Datas de inici i final als calendaris
        inici = self.calendariinici.get_date()
        fi = self.calendarifi.get_date()
        # Obtindre els strings de les datas amb barra en lloc de guions
        inicib = str(inici).replace("-", "/")
        fib = str(fi).replace("-", "/")
        # Obtindre el unix time de les datas
        unixinici = datetime.datetime.strptime(inicib, '%Y/%m/%d').strftime("%s")
        unixfi = datetime.datetime.strptime(fib, '%Y/%m/%d').strftime("%s")
        uri = 'http://127.0.0.1:3000/d/qox2hekgz/consum-aigua?orgId=1&from=' + unixinici + '999&to=' + unixfi + '000'
        self.p = subprocess.Popen(['chromium-browser', uri])

    def onBtGrafanaL_Clicked(self):
        # Datas de inici i final als calendaris
        inici = self.calendariinici.get_date()
        fi = self.calendarifi.get_date()
        # Obtindre els strings de les datas amb barra en lloc de guions
        inicib = str(inici).replace("-", "/")
        fib = str(fi).replace("-", "/")
        # Obtindre el unix time de les datas
        unixinici = datetime.datetime.strptime(inicib, '%Y/%m/%d').strftime("%s")
        unixfi = datetime.datetime.strptime(fib, '%Y/%m/%d').strftime("%s")
        uri = 'http://localhost:3000/d/riqt0ciRk/consum-llum?from=' + unixinici + '999&to=' + unixfi + '000'
        self.p = subprocess.Popen(['chromium-browser', uri])

        # self.p = subprocess.Popen(['chromium-browser','http://127.0.0.1:3000/d/qox2hekgz/consum-aigua?orgId=1&from=1621221340905&to=1621264540905'])

    # Iniciar el programa
